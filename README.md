### Welcome to my GitLab profile. 👋
**This is my favourite place to go.**
Legacy stuff and a similar page is also available at https://github.com/ckahlo.
You can also find me on <a rel="me" href="https://chaos.social/@ck1">Mastodon</a>.

On GitLab you may find my work for
*    [BMI PersoApp](https://gitlab.com/PersoApp)
*    [adesso SE](https://gitlab.com/adessoAG)
*    [VX4.NET](https://gitlab.com/VX4) (restricted access)
*   [Ringelpiez](https://gitlab.com/ringelpiez) (restricted access)



- 🔭 I’m currently working on mobile identity / eID (BSI TR-03110 meID / IVID) and integration with Optimos 2, Fidesmo and open platforms
- 🌱 I’m currently learning how to get several secure elements, eUICCs, USIMs, wearables under one hat (ideally :D)
- 👯 I’m looking to collaborate on secure element platforms for mobile identification in open, standardized environments
- 🤔 I’m looking for help with making people understand self-sovereign decentralized secure idendity conforming to international standards
- 💬 Ask me about ... uh, better don't do that. May be you won't like my answer. If you don't care, give it a try.
- 📫 How to reach me: drop a message here, on twitter, via keybase - please don't expect me to respond in realtime
- 😄 Pronouns: he/his 
- ⚡ Fun fact: there was a discussion and a bet ~20 years ago about "internet identities" ...

